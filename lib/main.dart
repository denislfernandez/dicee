import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.red,
        appBar: AppBar(
          backgroundColor: Colors.red,
          title: Text('Dicee'),
        ),
        body: Dice(),
      ),
    );
  }
}

class Dice extends StatefulWidget {
  @override
  _DiceState createState() => _DiceState();
}

class _DiceState extends State<Dice> {
  int leftDiceNumber;
  int rightDiceNumber;

  _DiceState() {
    leftDiceNumber = _getRandomNumber(1, 6);
    rightDiceNumber = _getRandomNumber(1, 6);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        children: <Widget>[
          DiceButton(
            assetUrl: 'assets/images/dice$leftDiceNumber.png',
            buttonCallback: _rollDices,
          ),
          DiceButton(
            assetUrl: 'assets/images/dice$rightDiceNumber.png',
            buttonCallback: _rollDices,
          ),
        ],
      ),
    );
  }

  void _rollDices() {
    setState(() {
      leftDiceNumber = _getRandomNumber(1, 6);
      rightDiceNumber = _getRandomNumber(1, 6);
    });
  }

  int _getRandomNumber(int begin, int end, {int excludeNumber = -1}) {
    int randomNumber = Random().nextInt(end);
    int fixedNumber = max(begin, randomNumber);
    while (fixedNumber == excludeNumber) {
      randomNumber = Random().nextInt(end);
      fixedNumber = max(begin, randomNumber);
    }
    return fixedNumber;
  }
}

class DiceButton extends StatelessWidget {
  final String assetUrl;
  final VoidCallback buttonCallback;

  DiceButton({this.assetUrl, this.buttonCallback});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: FlatButton(
        onPressed: buttonCallback,
        child: Image.asset(assetUrl),
      ),
    );
  }
}
